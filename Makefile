.PHONY: bom clean docker_build docker_run build output/version.txt board_render.png board_render_purple.png

GIT_VERSION := $(shell git describe --tags --always)
PYTHON := python3
OUTPUT_DIR := output
OUTPUT_FILE := $(OUTPUT_DIR)/beacon-$(GIT_VERSION).zip
LOGFILE?=$(OUTPUT_DIR)/kibot_error.log

all: build

clean:
	rm -rf output

docker_build: dockerbuild/Dockerfile
	cd dockerbuild && docker build -t kicad_ci . 
	docker tag kicad_ci registry.gitlab.com/nlighten-systems/beacon:kicad_ci

pushdockerimage: dockerbuild/Dockerfile
	docker login registry.gitlab.com
	docker push registry.gitlab.com/nlighten-systems/beacon:kicad_ci

docker_run:
	docker run --rm -it -u "$(shell id -u):$(shell id -g)" -v "$(abspath .):/data" kicad_ci /bin/bash

output:
	mkdir output

output/version.txt: | output
	echo "$(GIT_VERSION)" > $@

build:  | output
	kibot -g variant=BuckBoost -d output -c variants.kibot.yaml -e beacon/beacon.kicad_sch 2>> $(LOGFILE)
	kibot -g variant=Linear -d output -c variants.kibot.yaml -e beacon/beacon.kicad_sch 2>> $(LOGFILE)

