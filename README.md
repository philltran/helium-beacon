# Beacon

![image](beacon_render.jpg)

Small, portable, ruggedize private messaging mini-computer connected everywhere using Helium Network. Based off the [adafruit keyboard FeatherWing](https://www.adafruit.com/product/4818).

This hardware design is licensed open-source under the CERN-OHL-S v2 License. See LICENSE.txt.


## Requirements

 * Text privately between other Beacons and Web app client using Helium IoT Network for wireless communication.
 * Get approx geolocation (500m accuracy). Should work indoors also.
 * Keyboard - quick/easy typing
 * e-ink display - viewable in direct sun
 * Long battery life (3 days+). Charge with USB cable


### Radio

Semtech LR1110. [Reference design here](https://www.semtech.com/products/wireless-rf/lora-edge/lr1110trk1cks)


### Display

 * [Waveshare 2.66" e-ink](https://www.waveshare.com/wiki/2.66inch_e-Paper_Module)
 * [E2271CS021 2.7" e-ink 264x176](https://www.digikey.com/en/products/detail/pervasive-displays/E2271CS021/6821167)

[STM32-eink-display](https://blog.mike.norgate.xyz/stm32-eink-display-part-1-904f7d03e7b0)

### Keyboard

Blackberry Q10

https://github.com/arturo182/pmod_bbq10_keyboard

### Power Management

#### Fuel Guage

[Digikey](https://www.digikey.com/en/articles/fuel-gauge-ics-simplify-li-ion-cell-charge-monitoring)

 * [RT9426](https://www.richtek.com/assets/product_file/RT9426/DS9426-01.pdf)
 * [bq27210](https://www.ti.com/lit/ds/symlink/bq27010.pdf?HQS=dis-dk-null-digikeymode-dsf-pf-null-wwe&ts=1676164387803&ref_url=https%253A%252F%252Fwww.ti.com%252Fgeneral%252Fdocs%252Fsuppproductinfo.tsp%253FdistId%253D10%2526gotoUrl%253Dhttps%253A%252F%252Fwww.ti.com%252Flit%252Fgpn%252Fbq27010)
 * [LTC2942-1](https://www.analog.com/en/technical-articles/tiny-accurate-battery-gas-gauges-with-easy-to-use-i2c-interface.html) Doesn't estimate State of change (i2c registers have accumulated change, voltage, and temp only)
 * [BQ27427](https://www.ti.com/product/BQ27427)
 * [MAX17303]
 
#### Battery Charger

 * [XC6802](https://www.torexsemi.com/file/xc6802/XC6802.pdf)
 * [MCP73831/2]()
    used in [adafruit](https://cdn-learn.adafruit.com/assets/assets/000/041/630/original/feather_schem.png?1494449413)
#### Buck/Boost

 * [LTC3440](https://www.analog.com/en/technical-articles/it-just-got-easier-to-convert-lithium-ion-battery-voltage-to-33v.html)
 * [MAX1701](https://www.analog.com/en/design-notes/33v-lithiumioncell-buckboost-supply-requires-one-inductor.html)


### Note:

Based off the [AdaFruit featherwing](https://github.com/arturo182/keyboard_featherwing_hw)
